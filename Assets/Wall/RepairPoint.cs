﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------
// 修理対象の管理  
//----------------------------------------------------------------------------------------------------
public class RepairPoint : ObjectBase
{
	//--------------------------------------------------------------------------------
	// メンバ変数  
	//--------------------------------------------------------------------------------
	[SerializeField]float size = 2;		// サイズ 
		
	// 位置を教える 
	public Vector3 localPosition{ get{ return _t.localPosition; } } 

	//--------------------------------------------------------------------------------
	// 当たり判定 
	//--------------------------------------------------------------------------------
	public override bool IsObject(float x, float z)
	{
		Vector3 pos = localPosition;

		if(x < pos.x - size/2){ return false; }
		if(pos.x + size/2 < x){ return false; }
		if(z < pos.z - size/2){ return false; }
		if(pos.z + size/2 < z){ return false; }
		
		return true;
	}
}
