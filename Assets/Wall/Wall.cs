﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------
// 修理対象の管理  
//----------------------------------------------------------------------------------------------------
public class Wall : ObjectBase
{
	//--------------------------------------------------------------------------------
	// メンバ変数  
	//--------------------------------------------------------------------------------
	[SerializeField]Transform dmgAnchor = null;		// 修理する場所がでる位置 

	[SerializeField]float xSize = 2;	// 幅サイズ 
	[SerializeField]float zSize = 2;	// 奥行サイズ 

	[SerializeField]GameObject[] fxSmoke = null;	// 煙エフェクト 
		
	// 位置を教える 
	public Vector3 localPosition{ get{ return _t.localPosition; } } 
	public Vector3 dmgPosition  { get{ return dmgAnchor.position; } }

	[HideInInspector]
	public RepairPoint repairPoint  = null;		// 修理箇所の保持 
	public bool        isCollidOnly = false;	// 壊れない 

	float life = MAX_LIFETIME;	// 壊れるまでの時間 
	bool  isDamaged = false;	// 故障したフラグ 



	//--------------------------------------------------------------------------------
	// 壊れ進行  
	//--------------------------------------------------------------------------------
	private void Update()
	{
		if(repairPoint != null)
		{
			life -= Time.deltaTime;

			// 壊れる 
			if(life < 0)
			{
				Main.Miss();
				SoundManager.Play(SoundManager.SE.Bomb);
				GameObject.DestroyImmediate(this.gameObject);
				GameObject.DestroyImmediate(repairPoint.gameObject);
				return;
			}

			isDamaged = true;
		}
		else
		{
			if(isDamaged)
			{
				isDamaged = false;
				life = Mathf.Min(MAX_LIFETIME, life+REPAIR_LIFETIME);

				fxSmoke[0].SetActive(false);
				fxSmoke[1].SetActive(false);
				fxSmoke[2].SetActive(false);
			}
		}

		// エフェクト 
		if(life < 9.0f && !fxSmoke[0].activeSelf)
		{
			fxSmoke[0].SetActive(true);
		}
		if(life < 6.0f && !fxSmoke[1].activeSelf)
		{
			fxSmoke[1].SetActive(true);
		}
		if(life < 3.0f && !fxSmoke[2].activeSelf)
		{
			fxSmoke[2].SetActive(true);
		}
	}



	//--------------------------------------------------------------------------------
	// 当たり判定 
	//--------------------------------------------------------------------------------
	public override bool IsObject(float x, float z)
	{
		Vector3 pos = localPosition;

		if(x < pos.x - xSize/2){ return false; }
		if(pos.x + xSize/2 < x){ return false; }
		if(z < pos.z - zSize/2){ return false; }
		if(pos.z + zSize/2 < z){ return false; }
		
		return true;
	}

	//--------------------------------------------------------------------------------
	// 定数 
	//--------------------------------------------------------------------------------
	const float MAX_LIFETIME    = 10;
	const float REPAIR_LIFETIME = 3;
}
