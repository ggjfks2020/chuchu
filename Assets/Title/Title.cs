﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//----------------------------------------------------------------------------------------------------
// タイトル画面の管理 
//--------------------------------------------------------------------------------------------------
public class Title : MonoBehaviour
{
	//--------------------------------------------------------------------------------
	// メンバ変数  
	//--------------------------------------------------------------------------------
	[SerializeField]SpriteRenderer spStart = null;
	[SerializeField]SpriteRenderer spHelp  = null;

	[SerializeField]GameObject howToPlay = null;

	bool isStartSelected = true;



	//--------------------------------------------------------------------------------
	// 処理フロー  
	//--------------------------------------------------------------------------------
	IEnumerator Start()
	{
		SoundManager.Play(SoundManager.BGM.Title);
		FadeManager.FadeIn(time:2.5f);

		// ボタン待ち 
		while(true)
		{
			if(MultiInputManager.p1_button1    > 0){ break; }
			if(MultiInputManager.p1_button2    > 0){ break; }

			if(MultiInputManager.p1_buttonA    > 0){ break; }
			if(MultiInputManager.p1_buttonB    > 0){ break; }
			if(MultiInputManager.p1_buttonX    > 0){ break; }
			if(MultiInputManager.p1_buttonY    > 0){ break; }
			if(MultiInputManager.p1_buttonPlus > 0){ break; }

			if(MultiInputManager.p2_buttonA    > 0){ break; }
			if(MultiInputManager.p2_buttonB    > 0){ break; }
			if(MultiInputManager.p2_buttonX    > 0){ break; }
			if(MultiInputManager.p2_buttonY    > 0){ break; }
			if(MultiInputManager.p2_buttonPlus > 0){ break; }

			yield return null;
		}

		// 表示 
		UpdateButton();
		(spStart).gameObject.SetActive(true);
		(spHelp ).gameObject.SetActive(true);
		yield return null;

		// ボタン待ち 
		while(true)
		{
			if(MultiInputManager.p1_button1    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p1_button2    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }

			if(MultiInputManager.p1_buttonA    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p1_buttonB    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p1_buttonX    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p1_buttonY    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p1_buttonPlus == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }

			if(MultiInputManager.p2_buttonA    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p2_buttonB    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p2_buttonX    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p2_buttonY    == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }
			if(MultiInputManager.p2_buttonPlus == 1){ if(isStartSelected){ break; }else{ yield return ShowHelp(); } }

			if(MultiInputManager.p1_buttonLeft == 1 || MultiInputManager.p2_buttonLeft  == 1)
			{
				SoundManager.Play(SoundManager.SE.Kiss);
				isStartSelected = true;
				UpdateButton();
			}
			if(MultiInputManager.p1_buttonRight == 1 || MultiInputManager.p2_buttonRight  == 1)
			{
				SoundManager.Play(SoundManager.SE.Kiss);
				isStartSelected = false;
				UpdateButton();
			}

			yield return null;
		}

		SoundManager.Stop(SoundManager.BGM.Title);
		SoundManager.Play(SoundManager.SE.Repair);

		FadeManager.FadeOut();
		yield return FadeManager.Wait();

		SceneManager.LoadScene(GameSystem.SCENE.Stage1.ToString());
	}

	//--------------------------------------------------------------------------------
	// ボタン表示更新  
	//--------------------------------------------------------------------------------
	void UpdateButton()
	{
		(spStart).color = new Color(1, 1, 1, isStartSelected ? 1 : 0.5f);
		(spHelp ).color = new Color(1, 1, 1, isStartSelected ? 0.5f : 1);
	}

	//--------------------------------------------------------------------------------
	// ヘルプ表示  
	//--------------------------------------------------------------------------------
	IEnumerator ShowHelp()
	{
		SoundManager.Play(SoundManager.SE.Repair);
		howToPlay.SetActive(true);
		(spStart).gameObject.SetActive(false);
		(spHelp ).gameObject.SetActive(false);
		yield return null;

		while(true)
		{
			if(MultiInputManager.p1_button1    == 1){ break; }
			if(MultiInputManager.p1_button2    == 1){ break; }
															 
			if(MultiInputManager.p1_buttonA    == 1){ break; }
			if(MultiInputManager.p1_buttonB    == 1){ break; }
			if(MultiInputManager.p1_buttonX    == 1){ break; }
			if(MultiInputManager.p1_buttonY    == 1){ break; }
			if(MultiInputManager.p1_buttonPlus == 1){ break; }
															 
			if(MultiInputManager.p2_buttonA    == 1){ break; }
			if(MultiInputManager.p2_buttonB    == 1){ break; }
			if(MultiInputManager.p2_buttonX    == 1){ break; }
			if(MultiInputManager.p2_buttonY    == 1){ break; }
			if(MultiInputManager.p2_buttonPlus == 1){ break; }

			yield return null;
		}

		SoundManager.Play(SoundManager.SE.Repair);
		howToPlay.SetActive(false);
		(spStart).gameObject.SetActive(true);
		(spHelp ).gameObject.SetActive(true);
	}
}
