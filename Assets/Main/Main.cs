﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

//----------------------------------------------------------------------------------------------------
// メイン画面の管理  
//----------------------------------------------------------------------------------------------------
public class Main : MonoBehaviour
{
	//--------------------------------------------------------------------------------
	// メンバ変数  
	//--------------------------------------------------------------------------------
	static Main instance = null;	// シングルトンインスタンス 

	[SerializeField]PlayerCharacter p1 = null;	// 1P オスネズミ 
	[SerializeField]PlayerCharacter p2 = null;	// 2P メスネズミ 

	[SerializeField]GameObject repairBase = null;	// 修理ポイント複製元 

	[SerializeField]TextMeshProUGUI txmTimer = null;	// タイマー 
	[SerializeField]TextMeshProUGUI txmMiss  = null;	// ミスカウント 
	[SerializeField]GameObject      clear    = null;	// クリア画面 
	[SerializeField]GameObject      gameOver = null;	// ゲームオーバー画面 
	[SerializeField]Image           imgTitle = null;	// タイトルに戻る 
	[SerializeField]Image           imgEnd   = null;	// ゲーム終了 

	float gameTimer = 90;	// ゲーム時間 
	int   missCount = 3;	// 残機 

	bool isEndSelected = false;		// 終了が選ばれているか 



	//--------------------------------------------------------------------------------
	// コンストラクタ  
	//--------------------------------------------------------------------------------
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			GameObject.DestroyImmediate(this.gameObject);
			return ;
		}
	}



	//--------------------------------------------------------------------------------
	// 処理フロー  
	//--------------------------------------------------------------------------------
	IEnumerator Start()
	{
		MultiInputManager.isInputEnable = false;
		txmTimer.text = "";

		FadeManager.FadeIn();
		yield return FadeManager.Wait();

		// カウントダウン 
		SoundManager.Play(SoundManager.SE.Count);
		txmTimer.text = "3";
		yield return new WaitForSeconds(1);
		SoundManager.Play(SoundManager.SE.Count);
		txmTimer.text = "2";
		yield return new WaitForSeconds(1);
		SoundManager.Play(SoundManager.SE.Count);
		txmTimer.text = "1";
		yield return new WaitForSeconds(1);

		// 開始処理 
		{
			// 開始音 
			SoundManager.Play(SoundManager.SE.Start);

			// BGM再生  
			SoundManager.Play(SoundManager.BGM.Game);

			// 故障スポーン開始 
			StartCoroutine(AutoSpawn());

			// 入力可能に 
			MultiInputManager.isInputEnable = true;
		}

		// メインループ 
		while(gameTimer > 0 && missCount > 0)
		{
			txmTimer.text = string.Format(
				"{0}:{1}.{2}",
				(int)gameTimer / 60,
				(int)gameTimer % 60,
				(int)(gameTimer*100) % 100
			);

			txmMiss.text = string.Format(
				"{0} {1} {2}",
				missCount<3 ? "x" : " ",
				missCount<2 ? "x" : " ",
				missCount<1 ? "x" : " "
			);
			
			yield return null;
			gameTimer -= Time.deltaTime;
		}

		// 終了処理 
		{
			// BGM止める 
			SoundManager.Stop();

			// オブジェクトを破棄 
			GameSystem.GetObject<ObjectBase>((obj)=>
			{
				GameObject.DestroyImmediate(obj.gameObject);
			});
			GameObject.DestroyImmediate(p1.gameObject);
			GameObject.DestroyImmediate(p2.gameObject);
		}

		// ゲームオーバーだった場合 
		if(missCount <= 0)
		{
			// ゲームオーバーフローへ 
			yield return GameOver();
		}

		// クリアだった場合 
		if(gameTimer <= 0)
		{
			// クリアフローへ 
			yield return Clear();
		}
	}

	//--------------------------------------------------------------------------------
	// ゲームオーバー演出 
	//--------------------------------------------------------------------------------
	IEnumerator GameOver()
	{
		gameOver.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		SoundManager.Play(SoundManager.BGM.GameOver);

		yield return WaitNextButton();
	}

	//--------------------------------------------------------------------------------
	// クリア演出  
	//--------------------------------------------------------------------------------
	IEnumerator Clear()
	{
		clear.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		SoundManager.Play(SoundManager.BGM.Clear);

		yield return WaitNextButton();
	}

	//--------------------------------------------------------------------------------
	// メニューのボタン待ち   
	//--------------------------------------------------------------------------------
	IEnumerator WaitNextButton()
	{
		while(true)
		{
			if(MultiInputManager.p1_button1    == 1){ break; }
			if(MultiInputManager.p1_button2    == 1){ break; }
															 
			if(MultiInputManager.p1_buttonA    == 1){ break; }
			if(MultiInputManager.p1_buttonB    == 1){ break; }
			if(MultiInputManager.p1_buttonX    == 1){ break; }
			if(MultiInputManager.p1_buttonY    == 1){ break; }
			if(MultiInputManager.p1_buttonPlus == 1){ break; }
															 
			if(MultiInputManager.p2_buttonA    == 1){ break; }
			if(MultiInputManager.p2_buttonB    == 1){ break; }
			if(MultiInputManager.p2_buttonX    == 1){ break; }
			if(MultiInputManager.p2_buttonY    == 1){ break; }
			if(MultiInputManager.p2_buttonPlus == 1){ break; }

			yield return null;
		}
		
		// UI表示 
		(imgTitle).gameObject.SetActive(true);
		(imgEnd  ).gameObject.SetActive(true);
		UpdateButton();
		yield return null;

		while(true)
		{
			if(MultiInputManager.p1_button1    == 1){ break; }
			if(MultiInputManager.p1_button2    == 1){ break; }
															 
			if(MultiInputManager.p1_buttonA    == 1){ break; }
			if(MultiInputManager.p1_buttonB    == 1){ break; }
			if(MultiInputManager.p1_buttonX    == 1){ break; }
			if(MultiInputManager.p1_buttonY    == 1){ break; }
			if(MultiInputManager.p1_buttonPlus == 1){ break; }
															 
			if(MultiInputManager.p2_buttonA    == 1){ break; }
			if(MultiInputManager.p2_buttonB    == 1){ break; }
			if(MultiInputManager.p2_buttonX    == 1){ break; }
			if(MultiInputManager.p2_buttonY    == 1){ break; }
			if(MultiInputManager.p2_buttonPlus == 1){ break; }

			if(MultiInputManager.p1_buttonLeft == 1 || MultiInputManager.p2_buttonLeft  == 1)
			{
				SoundManager.Play(SoundManager.SE.Kiss);
				isEndSelected = false;
				UpdateButton();
			}
			if(MultiInputManager.p1_buttonRight == 1 || MultiInputManager.p2_buttonRight  == 1)
			{
				SoundManager.Play(SoundManager.SE.Kiss);
				isEndSelected = true;
				UpdateButton();
			}

			yield return null;
		}

		SoundManager.Play(SoundManager.SE.Repair);

		if(isEndSelected)
		{
			FadeManager.FadeOut(()=>{ Application.Quit(); }, 2.5f);
		}
		else
		{
			FadeManager.FadeOut(()=>{ SceneManager.LoadScene(GameSystem.SCENE.Title.ToString()); });
		}
	}
 
	//--------------------------------------------------------------------------------
	// ボタン表示更新  
	//--------------------------------------------------------------------------------
	void UpdateButton()
	{
		(imgTitle).color = new Color(1, 1, 1, isEndSelected ? 0.5f : 1);
		(imgEnd  ).color = new Color(1, 1, 1, isEndSelected ? 1 : 0.5f);
	}



	//--------------------------------------------------------------------------------
	// ミス    
	//--------------------------------------------------------------------------------
	public static void Miss()
	{
		instance.missCount--;
	}



	//--------------------------------------------------------------------------------
	// 故障生成  
	//--------------------------------------------------------------------------------
	IEnumerator AutoSpawn()
	{
		while(gameTimer > 0 && missCount > 0)
		{
			yield return new WaitForSeconds(Random.Range(0, 3.0f));
			SpawnRepairPoint();
		}
	}
	void SpawnRepairPoint()
	{
		// 壁をランダムに選出 
		List<Wall> lsWall = new List<Wall>();
		GameSystem.GetObject<Wall>((wall)=>
		{
			if(wall.isCollidOnly)       { return; }
			if(wall.repairPoint != null){ return; }
			lsWall.Add(wall);
		});
		if(lsWall.Count == 0){ return; }
		Wall target = lsWall[Random.Range(0, lsWall.Count)];

		// 故障オブジェクトの複製 
		GameObject obj = Instantiate<GameObject>(repairBase);
		Transform  t   = obj.transform;
		t.position     = target.dmgPosition;
		t.localScale   = Vector3.one;

		// ダメージを持たせる 
		RepairPoint dmg = obj.GetComponent<RepairPoint>();
		target.repairPoint = dmg;

		// 音 
		SoundManager.Play(SoundManager.SE.Damage);
	}



	//--------------------------------------------------------------------------------
	// DEBUG 
	//--------------------------------------------------------------------------------
	private void OnGUI()
	{
		if(GUILayout.Button("!"))
		{
			gameTimer = 0;
		}
	}
}
