﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------
// プレイヤーが操作するキャラクター  
//----------------------------------------------------------------------------------------------------
public class PlayerCharacter : MonoBehaviour
{
	//--------------------------------------------------------------------------------
	// 入力タイプ   
	//--------------------------------------------------------------------------------
	enum INPUT
	{
		Up,
		Down,
		Left,
		Right,
		Action,
		Walk
	}

	
	
	//--------------------------------------------------------------------------------
	// メンバ変数  
	//--------------------------------------------------------------------------------
	[SerializeField]Transform _t = null;	// 高速化 

	[SerializeField]int                   playerNum = 0;		// プレイヤー番号 
	[SerializeField]Transform             modelRoot = null;		// モデル 
	[SerializeField]Transform             headPos   = null;		// 頭 
	[SerializeField]Transform             oxiGauge  = null;		// 酸素ゲージ 
	[SerializeField]SpriteRendererIndexer oxiSprite = null;		// 酸素ゲージの画像処理 

	[SerializeField]Transform fxHeart = null;	// ハートエフェクト 
	[SerializeField]Transform fxSpana = null;	// スパナエフェクト 

	[SerializeField]PlayerCharacter pair = null;	// 相方 
	float       oxigen = MAX_OXIGEN;				// 酸素残量 
	RepairPoint target = null;						// 修理対象 

	// 前フレームの位置 
	Vector3 prePosition;

	// 位置や向きを教える 
	public Vector3 localPosition{ get{ return _t.localPosition; } } 
	public Vector3 localRotation{ get{ return modelRoot.localRotation.eulerAngles; } } 



	//--------------------------------------------------------------------------------
	// コンストラクタ     
	//--------------------------------------------------------------------------------
	private void Awake()
	{
		// 初期位置の影響で動かないようにする 
		prePosition = _t.localPosition;
	}

	//--------------------------------------------------------------------------------
	// 更新    
	//--------------------------------------------------------------------------------
	private void Update()
	{
		// 酸素減る 
		{
			oxigen = Mathf.Max(0, oxigen-Time.deltaTime);
			oxiGauge.localScale = new Vector3(oxigen/MAX_OXIGEN, 1, 1);
			oxiSprite.color = GetGaugeColor(oxigen/MAX_OXIGEN);
		}

		// 操作入力 
		Vector3 posBuffer = _t.localPosition;
		float   spd       = oxigen <= 0 ? SLOW_SPEED : SPEED;
		if(CheckInput(INPUT.Up))
		{
			posBuffer += new Vector3(0, 0, spd * Time.deltaTime);
		}
		if(CheckInput(INPUT.Down))
		{
			posBuffer -= new Vector3(0, 0, spd * Time.deltaTime);
		}
		if(CheckInput(INPUT.Left))
		{
			posBuffer -= new Vector3(spd * Time.deltaTime, 0, 0);
		}
		if(CheckInput(INPUT.Right))
		{
			posBuffer += new Vector3(spd * Time.deltaTime, 0, 0);
		}

		// 向き変更   
		ChangeDistance(posBuffer);

		// 相方判定 
		{
			// 重なる場所には移動不可能
			Vector3 dist = pair.localPosition - posBuffer;
			if(dist.magnitude < SIZE){ posBuffer = _t.localPosition; }

			// キス判定 
			CheckKiss();
		}

		// オブジェクト判定  
		{
			GameSystem.GetObject<ObjectBase>((obj)=>
			{
				// 重なる場所は移動不可能
				if(obj is Wall && obj.IsObject(posBuffer.x, posBuffer.z)){ posBuffer = _t.localPosition; }

				// 修理可能判定 
				if(obj is RepairPoint){	CheckRepair(obj as RepairPoint); }
			});
		}

		// 移動 
		_t.localPosition = posBuffer;

		// 座標更新 
		prePosition = _t.localPosition;
	}



	//--------------------------------------------------------------------------------
	// 向きを変える    
	//--------------------------------------------------------------------------------
	void ChangeDistance(Vector3 posBuffer)
	{
		Vector3 diff = posBuffer - prePosition;
		
		if(diff.x == 0 && diff.z >  0){ modelRoot.localRotation = Quaternion.Euler(0,   0, 0); }
		if(diff.x >  0 && diff.z >  0){	modelRoot.localRotation = Quaternion.Euler(0,  45, 0); }
		if(diff.x >  0 && diff.z == 0){	modelRoot.localRotation = Quaternion.Euler(0,  90, 0); }
		if(diff.x >  0 && diff.z <  0){	modelRoot.localRotation = Quaternion.Euler(0, 135, 0); }
		if(diff.x == 0 && diff.z <  0){	modelRoot.localRotation = Quaternion.Euler(0, 180, 0); }
		if(diff.x <  0 && diff.z <  0){	modelRoot.localRotation = Quaternion.Euler(0, 225, 0); }
		if(diff.x <  0 && diff.z == 0){	modelRoot.localRotation = Quaternion.Euler(0, 270, 0); }
		if(diff.x <  0 && diff.z >  0){	modelRoot.localRotation = Quaternion.Euler(0, 315, 0); }
	}



	//--------------------------------------------------------------------------------
	// 入力を受け付ける 
	//--------------------------------------------------------------------------------
	bool CheckInput(INPUT input)
	{
		switch(input)
		{
			case INPUT.Up    :{ return (MultiInputManager.p1_buttonUp    > 0 && playerNum == 1) || (MultiInputManager.p2_buttonUp    > 0 && playerNum == 2); }
			case INPUT.Down  :{ return (MultiInputManager.p1_buttonDown  > 0 && playerNum == 1) || (MultiInputManager.p2_buttonDown  > 0 && playerNum == 2); }
			case INPUT.Left  :{ return (MultiInputManager.p1_buttonLeft  > 0 && playerNum == 1) || (MultiInputManager.p2_buttonLeft  > 0 && playerNum == 2); }
			case INPUT.Right :{ return (MultiInputManager.p1_buttonRight > 0 && playerNum == 1) || (MultiInputManager.p2_buttonRight > 0 && playerNum == 2); }
			case INPUT.Action:
			{
				bool r = (MultiInputManager.p1_buttonA == 1 && playerNum == 1) || (MultiInputManager.p2_buttonA == 1 && playerNum == 2);
				r = r || (MultiInputManager.p1_buttonB == 1 && playerNum == 1) || (MultiInputManager.p2_buttonB == 1 && playerNum == 2);
				r = r || (MultiInputManager.p1_buttonX == 1 && playerNum == 1) || (MultiInputManager.p2_buttonX == 1 && playerNum == 2);
				r = r || (MultiInputManager.p1_buttonY == 1 && playerNum == 1) || (MultiInputManager.p2_buttonY == 1 && playerNum == 2);

				return r;
			}
		}

		return false;
	}



	//--------------------------------------------------------------------------------
	// チュー判定  
	//--------------------------------------------------------------------------------
	void CheckKiss()
	{
		// 距離判定 
		Vector3 dist = headPos.position - pair.localPosition;
		if(dist.magnitude > SIZE/2){ return; }
		
		// 向き判定 
		if(Mathf.Abs(localRotation.y - pair.localRotation.y) != 180){ return; }

		// エフェクト 
		if(!fxHeart.gameObject.activeSelf && oxigen+0.1f < MAX_OXIGEN)
		{
			StartCoroutine(FX_Heart());
		}
		
		// 酸素を回復 
		oxigen = MAX_OXIGEN;
	}



	//--------------------------------------------------------------------------------
	// 修理判定  
	//--------------------------------------------------------------------------------
	void CheckRepair(RepairPoint obj)
	{
		// 位置判定 
		Vector3 headPosition = headPos.position;
		if(!obj.IsObject(headPosition.x, headPosition.z)){ return; }

		// 修理 
		if(CheckInput(INPUT.Action))
		{
			// エフェクト 
			StartCoroutine(FX_Spana());

			// 修理 
			GameObject.DestroyImmediate(obj.gameObject);
			oxigen -= USE_OXIGEN;
		}
	}



	//--------------------------------------------------------------------------------
	// ゲージ色を取得  
	//--------------------------------------------------------------------------------
	Color GetGaugeColor(float rate)
	{
		float r = Mathf.Clamp(2-rate*2 , 0, 1);
		float g = Mathf.Clamp(  rate*2 , 0, 1);

		return new Color(r, g, 0);
	}



	//--------------------------------------------------------------------------------
	// ハートを出す  
	//--------------------------------------------------------------------------------
	IEnumerator FX_Heart()
	{
		SoundManager.Play(SoundManager.SE.Kiss);

		fxHeart.gameObject.SetActive(true);

		float timer = 0.5f;
		while(timer > 0)
		{
			float y =  Mathf.Sqrt(Mathf.Sqrt(1 - timer * 2)) * 2;
			fxHeart.localPosition = new Vector3(0, y, 0);

			yield return null;
			timer -= Time.deltaTime;
		}

		fxHeart.gameObject.SetActive(false);
	}
	//--------------------------------------------------------------------------------
	// スパナを出す  
	//--------------------------------------------------------------------------------
	IEnumerator FX_Spana()
	{
		SoundManager.Play(SoundManager.SE.Repair);

		fxSpana.gameObject.SetActive(true);

		float timer = 0.5f;
		while(timer > 0)
		{
			float y =  Mathf.Sqrt(Mathf.Sqrt(1 - timer * 2)) * 2;
			fxSpana.localPosition = new Vector3(0, y, 0);

			yield return null;
			timer -= Time.deltaTime;
		}

		fxSpana.gameObject.SetActive(false);
	}


	//--------------------------------------------------------------------------------
	// 定数  
	//--------------------------------------------------------------------------------
	const float SPEED      = 30;
	const float SLOW_SPEED = 0.5f;
	const float MAX_OXIGEN = 30;
	const float USE_OXIGEN = 5;
	const float SIZE       = 1;
}
