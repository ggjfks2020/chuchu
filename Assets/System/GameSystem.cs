﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//----------------------------------------------------------------------------------------------------
// ゲームシステム 
//----------------------------------------------------------------------------------------------------
public class GameSystem : MonoBehaviour
{
	//--------------------------------------------------------------------------------
	// シーン名  
	//--------------------------------------------------------------------------------
	public enum SCENE
	{
		Title,
		StageSelect,
		Stage0,
		Stage1,
		Stage2,
		Stage3,
		Stage4,
		Stage5,
	}
		
		
		
	//--------------------------------------------------------------------------------
	// メンバ変数 
	//--------------------------------------------------------------------------------
	static GameSystem instance;	// シングルトンインスタンス 

	// オブジェクトリスト 
	List<ObjectBase> lsObj = new List<ObjectBase>();
	List<ObjectBase> lsAdd = new List<ObjectBase>();
	List<ObjectBase> lsRem = new List<ObjectBase>();


	
	//--------------------------------------------------------------------------------
	// コンストラクタ 
	//--------------------------------------------------------------------------------
	void Awake()
	{
		if(instance == null)
		{
			instance = this;
			GameObject.DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			GameObject.DestroyImmediate(this.gameObject);
			return;
		}

		SceneManager.LoadScene(SCENE.Title.ToString());
	}

	//--------------------------------------------------------------------------------
	// 更新   
	//--------------------------------------------------------------------------------
	void LateUpdate()
	{
		// nullオブジェクトをリストから除外 
		for(int i=lsObj.Count-1; i>=0; i--)
		{
			if(lsObj[i] == null){ lsObj.RemoveAt(i); }
		}

		// 除外リストを処理 
		foreach(ObjectBase obj in lsRem)
		{
			if(lsObj.Contains(obj))
			{
				lsObj.Remove(obj);
			}
		}
		lsRem.Clear();

		// 追加リストを処理 
		foreach(ObjectBase obj in lsAdd)
		{
			if(!lsObj.Contains(obj))
			{
				lsObj.Add(obj);
			}
		}
		lsAdd.Clear();
	}

	//--------------------------------------------------------------------------------
	// オブジェクト追加   
	//--------------------------------------------------------------------------------
	public static void AddObject(ObjectBase obj)
	{
		instance.lsAdd.Add(obj);
	}

	//--------------------------------------------------------------------------------
	// オブジェクト除外 
	//--------------------------------------------------------------------------------
	public static void RemoveObject(ObjectBase obj)
	{
		instance.lsRem.Add(obj);
	}

	//--------------------------------------------------------------------------------
	// オブジェクトを取得  
	//--------------------------------------------------------------------------------
	public static void GetObject<T>(System.Action<T> Return) where T : ObjectBase
	{
		if(instance == null){ return; }

		foreach(ObjectBase obj in instance.lsObj)
		{
			// nullがあったら実行しない 
			if(obj==null){ continue; }

			// オブジェクトを返す 
			if(obj is T){ Return(obj as T); }
		}
	}
}
