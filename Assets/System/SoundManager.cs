﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------
// サウンド周りを管理する  
//----------------------------------------------------------------------------------------------------
public class SoundManager : MonoBehaviour
{
	//--------------------------------------------------------------------------------
	// SEリスト  
	//--------------------------------------------------------------------------------
	public enum SE
	{
		Bomb,		// 爆破 
		Kiss,		// チュー 
		Repair,		// 修理 
		Down,		// 酸欠 
		Damage,		// 故障中 
		Count,		// カウントダウン  
		Start,		// ゲーム開始 
	}



	//--------------------------------------------------------------------------------
	// BGMリスト  
	//--------------------------------------------------------------------------------
	public enum BGM
	{
		Title,		// タイトル画面 
		Game,		// ゲーム中 
		Clear,		// クリア(ジングル)
		GameOver,	// ゲームオーバー(ジングル) 
	}



	//--------------------------------------------------------------------------------
	// BGM用クリップクラス 
	//--------------------------------------------------------------------------------
	[System.Serializable]
	public class MusicClip
	{
		public AudioClip intro;
		public AudioClip loop;
	}



	//--------------------------------------------------------------------------------
	// メンバ変数 
	//--------------------------------------------------------------------------------
	static SoundManager instance;   // シングルトンインスタンス 

	// 再生参照用リスト 
	Dictionary< SE, AudioSource[]>  seList = new Dictionary< SE, AudioSource[]>();
	Dictionary<BGM, AudioSource[]> bgmList = new Dictionary<BGM, AudioSource[]>();

	// 素材アタッチ 
	[SerializeField]AudioClip[]  seResources = new AudioClip[System.Enum.GetNames(typeof(SE )).Length];
	[SerializeField]MusicClip[] bgmResources = new MusicClip[System.Enum.GetNames(typeof(BGM)).Length];

	// BGMの再生を管理しているコルーチン 
	Coroutine bgmPlayer = null;



	//--------------------------------------------------------------------------------
	// コンストラクタ 
	//--------------------------------------------------------------------------------
	void Awake()
	{
		if(instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);

			Initialize();
			//OnApplicationPause(false);
		}
		else
		{
			GameObject.DestroyImmediate(this.gameObject);
		}
	}

	//--------------------------------------------------------------------------------
	// 初期化  
	//--------------------------------------------------------------------------------
	public void Initialize()
	{
		// 前に何かあったら削除 
		for(int i=this.gameObject.transform.childCount-1; i>=0; i--)
		{
			GameObject.DestroyImmediate(this.gameObject.transform.GetChild(i));
		}

		// SE制御機構構築 
		for(int i=0; i<seResources.Length; i++)
		{
			SE se = (SE)i;
			GameObject obj = new GameObject(se.ToString());
			obj.transform.parent = this.gameObject.transform;

			for(int j=0; j<MAX_SE_CHANNELS; j++)
			{
				AudioSource a = obj.AddComponent<AudioSource>();
				a.clip = seResources[i];
				a.playOnAwake = false;
			}

			AudioSource[] sources = obj.GetComponents<AudioSource>();
			seList[se] = sources;
		}

		// BGM制御機構構築
		for(int i=0; i<bgmResources.Length; i++)
		{
			BGM bgm = (BGM)i;
			GameObject obj = new GameObject(bgm.ToString());
			obj.transform.parent = this.gameObject.transform;

			AudioSource intro = obj.AddComponent<AudioSource>();
			AudioSource loop  = obj.AddComponent<AudioSource>();
			intro.clip = bgmResources[i].intro;
			loop.clip  = bgmResources[i].loop;
			intro.playOnAwake = false;
			loop.playOnAwake  = false;
			intro.loop = false;
			loop.loop  = true;

			AudioSource[] sources = { intro, loop };
			bgmList[bgm] = sources;
		}
	}

#region SE
	//--------------------------------------------------------------------------------
	// SE再生   
	//--------------------------------------------------------------------------------
	public static void Play(SE id)
	{
		if(instance == null){ return; }
		instance.Play_(id);
	}
	void Play_(SE id)
	{
		// 未使用のソースで再生 
		// 未使用がなければ最も古いソースで再生 
		int index = 0;
		AudioSource[] sourcelist = seList[id];
		for(int i=0; i<sourcelist.Length; i++)
		{
			// 再生中じゃないものがあったら確定 
			if(!sourcelist[i].isPlaying)
			{
				index = i;
				break;
			}

			// 再生時間を比較して古いものを記憶 
			if(sourcelist[i].time > sourcelist[index].time)
			{
				index = i;
			}
		}

		// AudioSourceを再生 
		if(sourcelist[index].gameObject.activeSelf)
		{
			sourcelist[index].Play();
		}
	}

	//--------------------------------------------------------------------------------
	// SE停止 
	//--------------------------------------------------------------------------------
	public static void Stop(SE id)
	{
		if(instance == null){ return; }
		instance.Stop_(id);
	}
	void Stop_(SE id)
	{
		AudioSource[] sourcelist = seList[id];
		for(int i=0; i<sourcelist.Length; i++)
		{
			sourcelist[i].Stop();
		}
	}

	//--------------------------------------------------------------------------------
	// SE再生中チェック 
	//--------------------------------------------------------------------------------
	public static bool IsPlaying(SE id)
	{
		if(instance == null){ return false; }
		return instance.IsPlaying_(id);
	}
	bool IsPlaying_(SE id)
	{
		bool result = false;
		AudioSource[] sourcelist = seList[id];
		for(int i=0; i<sourcelist.Length; i++)
		{
			if(sourcelist[i].isPlaying)
			{
				result = true;
			}
		}
		return result;
	}
#endregion

#region BGM
	//--------------------------------------------------------------------------------
	// BGM再生   
	//--------------------------------------------------------------------------------
	public static void Play(BGM id)
	{
		if(instance == null){ return; }
		instance.Play_(id);
	}
	void Play_(BGM id)
	{
		// すでになっていたら止める 
		if(bgmPlayer != null){ StopCoroutine(bgmPlayer); }
		AudioSource[] sourcelist = bgmList[id];
		for(int i=0; i<sourcelist.Length; i++)
		{
			sourcelist[i].Stop();
		}

		// 再生コルーチンを動かす 
		bgmPlayer = StartCoroutine(PlayBGM(id));
	}
	IEnumerator PlayBGM(BGM id)
	{
		if(bgmList[id][0].gameObject.activeSelf)
		{
			bgmList[id][0].Play();
		}
		else
		{
			yield break;
		}

		while(bgmList[id][0].isPlaying){ yield return null; }

		if(bgmList[id][1].gameObject.activeSelf)
		{
			bgmList[id][1].Play();
		}
		else
		{
			yield break;
		}
	}

	//--------------------------------------------------------------------------------
	// BGM停止 
	//--------------------------------------------------------------------------------
	public static void Stop()
	{
		if(instance == null){ return; }

		for(int i=0; i<instance.bgmResources.Length; i++)
		{
			instance.Stop_((BGM)i);
		}
	}
	public static void Stop(BGM id)
	{
		if(instance == null){ return; }
		instance.Stop_(id);
	}
	void Stop_(BGM id)
	{
		if(bgmPlayer != null){ StopCoroutine(bgmPlayer); }
		AudioSource[] sourcelist = bgmList[id];
		for(int i=0; i<sourcelist.Length; i++)
		{
			sourcelist[i].Stop();
		}
	}

	//--------------------------------------------------------------------------------
	// BGM停止(すべて停止) 
	//--------------------------------------------------------------------------------
	public static void FadeOut(float time=DEFAULT_FADE_TIME)
	{
		if(instance == null){ return; }
		instance.FadeOut_(time);
	}
	void FadeOut_(float time)
	{
		for(int i=0; i<bgmResources.Length; i++)
		{
			BGM id = (BGM)i; 

			// 鳴っているものがあれば止める 
			if(IsPlaying_(id))
			{
				StartCoroutine(FadeOutBGM(id, time));
			}
		}
	}
	IEnumerator FadeOutBGM(BGM id, float maxTime)
	{
		float timer = 0;
		while(timer < maxTime)
		{
			float volume = 1.0f - (timer / maxTime);

			SetVolumeBGM(id, volume);

			yield return null;
			timer += Time.deltaTime;
		}
		Stop_(id);
		SetVolumeBGM(id, 1.0f);
	}

	//--------------------------------------------------------------------------------
	// BGMの音量セット 
	//--------------------------------------------------------------------------------
	void SetVolumeBGM(BGM id, float volume)
	{
#if UNITY_IOS && !UNITY_EDITOR
		volume = NativeCall.IsPlayingOtherMusic() ? 0 : volume;
#endif
		bgmList[id][0].volume = volume;
		bgmList[id][1].volume = volume;
	}

	//--------------------------------------------------------------------------------
	// BGM再生中チェック 
	//--------------------------------------------------------------------------------
	public static bool IsPlaying(BGM id)
	{
		if(instance == null){ return false; }
		return instance.IsPlaying_(id);
	}
	bool IsPlaying_(BGM id)
	{
		bool result = false;
		AudioSource[] sourcelist = bgmList[id];
		for(int i=0; i<sourcelist.Length; i++)
		{
			if(sourcelist[i].isPlaying)
			{
				result = true;
			}
		}
		return result;
	}
#endregion

	//--------------------------------------------------------------------------------
	// サウンド オン/オフ 
	//--------------------------------------------------------------------------------
	public static void SetSoundEnable(bool val)
	{
		if(instance == null){ return; }

		for(int i=0; i<instance.gameObject.transform.childCount; i++)
		{
			GameObject obj = instance.gameObject.transform.GetChild(i).gameObject;
			obj.SetActive(val);
		}
	}

	//--------------------------------------------------------------------------------
	// サスペンドリジューム時に実行 
	// 他のアプリでサウンドを鳴らしていたらBGMとSEを止める 
	//--------------------------------------------------------------------------------
	//void OnApplicationPause(bool isPause)
	//{
	//	if(!isPause)
	//	{
	//		foreach(AudioSource[] audios in bgmList.Values)
	//		{
	//			for(int i=0; i<audios.Length; i++)
	//			{
	//				audios[i].volume = NativeCall.IsPlayingOtherMusic() ? 0 : 1;
	//			}
	//		}
	//		foreach(AudioSource[] audios in seList.Values)
	//		{
	//			for(int i=0; i<audios.Length; i++)
	//			{
	//				audios[i].volume = NativeCall.IsPlayingOtherMusic() ? 0 : 1;
	//			}
	//		}
	//	}
	//}

	//--------------------------------------------------------------------------------
	// 定数   
	//--------------------------------------------------------------------------------
	const int   MAX_SE_CHANNELS   = 1;		// SE同時再生数 
	const float DEFAULT_FADE_TIME = 0.25f;	// フェードアウトにかかる時間 
}
