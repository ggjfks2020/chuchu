﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------
// フェードの管理　 
//----------------------------------------------------------------------------------------------------
public class FadeManager : MonoBehaviour
{
	//--------------------------------------------------------------------------------
	// メンバ変数  
	//--------------------------------------------------------------------------------
	static FadeManager instance;

	[SerializeField]SpriteRenderer mask;

	Coroutine coroutine = null;

	public static bool isOpen{ get; private set; }
	public static bool isAnim{ get; private set; }



	//--------------------------------------------------------------------------------
	// コンストラクタ  
	//--------------------------------------------------------------------------------
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
			GameObject.DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			GameObject.DestroyImmediate(this.gameObject);
		}
	}



	//--------------------------------------------------------------------------------
	// フェードイン 
	//--------------------------------------------------------------------------------
	public static Coroutine FadeIn(System.Action Callback=null, float time=DEFAUL_FADETIME)
	{
		return FadeIn(Callback, Color.black, time);
	}
	public static Coroutine FadeIn(System.Action Callback, Color color, float time=DEFAUL_FADETIME)
	{
		if(instance == null){ return null; }

		isOpen = true;
		if(instance.coroutine != null){ instance.StopCoroutine(instance.coroutine); }
		instance.coroutine = instance.StartCoroutine(instance.Fade(true, Callback, color, time));
		return instance.coroutine;
	}



	//--------------------------------------------------------------------------------
	// フェードアウト 
	//--------------------------------------------------------------------------------
	public static Coroutine FadeOut(System.Action Callback=null, float time=DEFAUL_FADETIME)
	{
		return FadeOut(Callback, Color.black, time);
	}
	public static Coroutine FadeOut(System.Action Callback, Color color, float time=DEFAUL_FADETIME)
	{
		if(instance == null){ return null; }

		isOpen = false;
		if(instance.coroutine != null){ instance.StopCoroutine(instance.coroutine); }
		instance.coroutine = instance.StartCoroutine(instance.Fade(false, Callback, color, time));
		return instance.coroutine;
	}



	//--------------------------------------------------------------------------------
	// フェード処理  
	//--------------------------------------------------------------------------------
	IEnumerator Fade(bool isFadeIn, System.Action Callback, Color color, float time)
	{
		isAnim = true;

		float timer = 0;
		while(timer < time)
		{
			float alpha = isFadeIn ? (1 - timer / time) : (timer / time);
			mask.color = new Color(color.r, color.g, color.b, alpha);

			yield return null;
			timer += Time.deltaTime;
		}
		
		mask.color = new Color(color.r, color.g, color.b, (isFadeIn ? 0 : 1));  
		if(Callback != null){ Callback(); }

		isAnim = false;
	}



	//--------------------------------------------------------------------------------
	// 待ち  
	//--------------------------------------------------------------------------------
	public static Coroutine Wait()
	{
		if(instance == null){ return null; }
		
		return instance.coroutine;
	}



	//--------------------------------------------------------------------------------
	// 定数  
	//--------------------------------------------------------------------------------
	const float DEFAUL_FADETIME = 0.5f;
}
