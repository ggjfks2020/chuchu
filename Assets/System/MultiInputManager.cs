﻿// MultiInputManager.cs
//
// @idev Unity2018.3.3f1 / VisualStudio2017
// @auth FCEI.No-Va
// @date 2019/02/25
//------------------------------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------
// 色々な入力をサポート  
//----------------------------------------------------------------------------------------------------
public class MultiInputManager : MonoBehaviour
{
	//--------------------------------------------------------------------------------
	// メンバ変数 
	//--------------------------------------------------------------------------------
	static MultiInputManager[] instance = new MultiInputManager[4];		// シングルトンインスタンス 
	int[] buf = new int[18];											// バッファ 
	private static bool _isInputEaable = true; 
	public  static bool  isInputEnable
	{
		get{ return _isInputEaable;  }
		set{ _isInputEaable = value; ClearInputBufffer(); }
	}
	[SerializeField]int controllerNum = 1;



	//--------------------------------------------------------------------------------
	// 入力応答 
	//--------------------------------------------------------------------------------
	public static int p1_button1    { get{ return instance[0] == null ? 0 : instance[0].buf[ 0]; } }
	public static int p1_button2    { get{ return instance[0] == null ? 0 : instance[0].buf[ 1]; } }
	public static int p1_buttonUp   { get{ return instance[0] == null ? 0 : instance[0].buf[ 2]; } }
	public static int p1_buttonDown { get{ return instance[0] == null ? 0 : instance[0].buf[ 3]; } }
	public static int p1_buttonLeft { get{ return instance[0] == null ? 0 : instance[0].buf[ 4]; } }
	public static int p1_buttonRight{ get{ return instance[0] == null ? 0 : instance[0].buf[ 5]; } }
	public static int p1_buttonA    { get{ return instance[0] == null ? 0 : instance[0].buf[ 6]; } }
	public static int p1_buttonB    { get{ return instance[0] == null ? 0 : instance[0].buf[ 7]; } }
	public static int p1_buttonX    { get{ return instance[0] == null ? 0 : instance[0].buf[ 8]; } }
	public static int p1_buttonY    { get{ return instance[0] == null ? 0 : instance[0].buf[ 9]; } }
	public static int p1_buttonL    { get{ return instance[0] == null ? 0 : instance[0].buf[10]; } }
	public static int p1_buttonR    { get{ return instance[0] == null ? 0 : instance[0].buf[11]; } }
	public static int p1_buttonLZ   { get{ return instance[0] == null ? 0 : instance[0].buf[12]; } }
	public static int p1_buttonRZ   { get{ return instance[0] == null ? 0 : instance[0].buf[13]; } }
	public static int p1_buttonLS   { get{ return instance[0] == null ? 0 : instance[0].buf[14]; } }
	public static int p1_buttonRS   { get{ return instance[0] == null ? 0 : instance[0].buf[15]; } }
	public static int p1_buttonPlus { get{ return instance[0] == null ? 0 : instance[0].buf[16]; } }
	public static int p1_buttonMinus{ get{ return instance[0] == null ? 0 : instance[0].buf[17]; } }

	public static int p2_button1    { get{ return instance[1] == null ? 0 : instance[1].buf[ 0]; } }
	public static int p2_button2    { get{ return instance[1] == null ? 0 : instance[1].buf[ 1]; } }
	public static int p2_buttonUp   { get{ return instance[1] == null ? 0 : instance[1].buf[ 2]; } }
	public static int p2_buttonDown { get{ return instance[1] == null ? 0 : instance[1].buf[ 3]; } }
	public static int p2_buttonLeft { get{ return instance[1] == null ? 0 : instance[1].buf[ 4]; } }
	public static int p2_buttonRight{ get{ return instance[1] == null ? 0 : instance[1].buf[ 5]; } }
	public static int p2_buttonA    { get{ return instance[1] == null ? 0 : instance[1].buf[ 6]; } }
	public static int p2_buttonB    { get{ return instance[1] == null ? 0 : instance[1].buf[ 7]; } }
	public static int p2_buttonX    { get{ return instance[1] == null ? 0 : instance[1].buf[ 8]; } }
	public static int p2_buttonY    { get{ return instance[1] == null ? 0 : instance[1].buf[ 9]; } }
	public static int p2_buttonL    { get{ return instance[1] == null ? 0 : instance[1].buf[10]; } }
	public static int p2_buttonR    { get{ return instance[1] == null ? 0 : instance[1].buf[11]; } }
	public static int p2_buttonLZ   { get{ return instance[1] == null ? 0 : instance[1].buf[12]; } }
	public static int p2_buttonRZ   { get{ return instance[1] == null ? 0 : instance[1].buf[13]; } }
	public static int p2_buttonLS   { get{ return instance[1] == null ? 0 : instance[1].buf[14]; } }
	public static int p2_buttonRS   { get{ return instance[1] == null ? 0 : instance[1].buf[15]; } }
	public static int p2_buttonPlus { get{ return instance[1] == null ? 0 : instance[1].buf[16]; } }
	public static int p2_buttonMinus{ get{ return instance[1] == null ? 0 : instance[1].buf[17]; } }

	public static int p3_button1    { get{ return instance[2] == null ? 0 : instance[2].buf[ 0]; } }
	public static int p3_button2    { get{ return instance[2] == null ? 0 : instance[2].buf[ 1]; } }
	public static int p3_buttonUp   { get{ return instance[2] == null ? 0 : instance[2].buf[ 2]; } }
	public static int p3_buttonDown { get{ return instance[2] == null ? 0 : instance[2].buf[ 3]; } }
	public static int p3_buttonLeft { get{ return instance[2] == null ? 0 : instance[2].buf[ 4]; } }
	public static int p3_buttonRight{ get{ return instance[2] == null ? 0 : instance[2].buf[ 5]; } }
	public static int p3_buttonA    { get{ return instance[2] == null ? 0 : instance[2].buf[ 6]; } }
	public static int p3_buttonB    { get{ return instance[2] == null ? 0 : instance[2].buf[ 7]; } }
	public static int p3_buttonX    { get{ return instance[2] == null ? 0 : instance[2].buf[ 8]; } }
	public static int p3_buttonY    { get{ return instance[2] == null ? 0 : instance[2].buf[ 9]; } }
	public static int p3_buttonL    { get{ return instance[2] == null ? 0 : instance[2].buf[10]; } }
	public static int p3_buttonR    { get{ return instance[2] == null ? 0 : instance[2].buf[11]; } }
	public static int p3_buttonLZ   { get{ return instance[2] == null ? 0 : instance[2].buf[12]; } }
	public static int p3_buttonRZ   { get{ return instance[2] == null ? 0 : instance[2].buf[13]; } }
	public static int p3_buttonLS   { get{ return instance[2] == null ? 0 : instance[2].buf[14]; } }
	public static int p3_buttonRS   { get{ return instance[2] == null ? 0 : instance[2].buf[15]; } }
	public static int p3_buttonPlus { get{ return instance[2] == null ? 0 : instance[2].buf[16]; } }
	public static int p3_buttonMinus{ get{ return instance[2] == null ? 0 : instance[2].buf[17]; } }

	public static int p4_button1    { get{ return instance[3] == null ? 0 : instance[3].buf[ 0]; } }
	public static int p4_button2    { get{ return instance[3] == null ? 0 : instance[3].buf[ 1]; } }
	public static int p4_buttonUp   { get{ return instance[3] == null ? 0 : instance[3].buf[ 2]; } }
	public static int p4_buttonDown { get{ return instance[3] == null ? 0 : instance[3].buf[ 3]; } }
	public static int p4_buttonLeft { get{ return instance[3] == null ? 0 : instance[3].buf[ 4]; } }
	public static int p4_buttonRight{ get{ return instance[3] == null ? 0 : instance[3].buf[ 5]; } }
	public static int p4_buttonA    { get{ return instance[3] == null ? 0 : instance[3].buf[ 6]; } }
	public static int p4_buttonB    { get{ return instance[3] == null ? 0 : instance[3].buf[ 7]; } }
	public static int p4_buttonX    { get{ return instance[3] == null ? 0 : instance[3].buf[ 8]; } }
	public static int p4_buttonY    { get{ return instance[3] == null ? 0 : instance[3].buf[ 9]; } }
	public static int p4_buttonL    { get{ return instance[3] == null ? 0 : instance[3].buf[10]; } }
	public static int p4_buttonR    { get{ return instance[3] == null ? 0 : instance[3].buf[11]; } }
	public static int p4_buttonLZ   { get{ return instance[3] == null ? 0 : instance[3].buf[12]; } }
	public static int p4_buttonRZ   { get{ return instance[3] == null ? 0 : instance[3].buf[13]; } }
	public static int p4_buttonLS   { get{ return instance[3] == null ? 0 : instance[3].buf[14]; } }
	public static int p4_buttonRS   { get{ return instance[3] == null ? 0 : instance[3].buf[15]; } }
	public static int p4_buttonPlus { get{ return instance[3] == null ? 0 : instance[3].buf[16]; } }
	public static int p4_buttonMinus{ get{ return instance[3] == null ? 0 : instance[0].buf[17]; } }

	[SerializeField]string axisVertical   = "Vertical";
	[SerializeField]string axisHorizontal = "Horizontal";
	[SerializeField]string buttonA        = "A";
	[SerializeField]string buttonB        = "B";
	[SerializeField]string buttonX        = "X";
	[SerializeField]string buttonY        = "Y";
	[SerializeField]string buttonL        = "L";
	[SerializeField]string buttonR        = "R";
	[SerializeField]string buttonLZ       = "LZ";
	[SerializeField]string buttonRZ       = "RZ";
	[SerializeField]string buttonLS       = "LS";
	[SerializeField]string buttonRS       = "RS";
	[SerializeField]string buttonPlus     = "PLUS";
	[SerializeField]string buttonMinus    = "MINUS";

	[SerializeField]KeyCode keyCodeUp    = KeyCode.UpArrow;
	[SerializeField]KeyCode keyCodeDown  = KeyCode.DownArrow;
	[SerializeField]KeyCode keyCodeLeft  = KeyCode.LeftAlt;
	[SerializeField]KeyCode keyCodeRight = KeyCode.RightAlt;
	[SerializeField]KeyCode keyCodeA     = KeyCode.Z;
	[SerializeField]KeyCode keyCodeB     = KeyCode.V;
	[SerializeField]KeyCode keyCodeX     = KeyCode.X;
	[SerializeField]KeyCode keyCodeY     = KeyCode.Y;
	[SerializeField]KeyCode keyCodeL     = KeyCode.A;
	[SerializeField]KeyCode keyCodeR     = KeyCode.F;
	[SerializeField]KeyCode keyCodeLZ    = KeyCode.Q;
	[SerializeField]KeyCode keyCodeRZ    = KeyCode.R;
	[SerializeField]KeyCode keyCodeLS    = KeyCode.S;
	[SerializeField]KeyCode keyCodeRS    = KeyCode.D;
	[SerializeField]KeyCode keyCodePlus  = KeyCode.E;
	[SerializeField]KeyCode keyCodeMinus = KeyCode.W;



	//--------------------------------------------------------------------------------
	// 初期化  
	//--------------------------------------------------------------------------------
	void Awake()
	{
		int index = controllerNum - 1;

		if(instance[index] == null)
		{
			instance[index] = this;
			GameObject.DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			GameObject.Destroy(this.gameObject);
			return;
		}
	}



	//--------------------------------------------------------------------------------
	// 更新  
	//--------------------------------------------------------------------------------
	void Update()
    {
		// 入力させない 
		if(!isInputEnable){ return; }

		// ボタン1
		{
			bool input = false;
			if(Input.GetMouseButton(0)        ){ input = true; }
		    if(Input.GetKey(KeyCode.LeftShift)){ input = true; }
			if(input){ buf[0]++; }else{ buf[0]=0; }
		}
  
		// ボタン2
		{
			bool input = false;
			if(Input.GetMouseButton(1)    ){ input = true; }
		    if(Input.GetKey(KeyCode.Space)){ input = true; }
			if(input){ buf[1]++; }else{ buf[1]=0; }
		}	

		// 上 
		{
			bool input = false;
			if(Input.GetAxisRaw(axisVertical)>0){ input = true; }
		    if(Input.GetKey(keyCodeUp)){ input = true; }
			if(input){ buf[2]++; }else{ buf[2]=0; }
		}

		// 下　
		{
			bool input = false;
			if(Input.GetAxisRaw(axisVertical)<0){ input = true; }
		    if(Input.GetKey(keyCodeDown)){ input = true; }
			if(input){ buf[3]++; }else{ buf[3]=0; }
		}

		// 左 
		{
			bool input = false;
			if(Input.GetAxisRaw(axisHorizontal)<0){ input = true; }
		    if(Input.GetKey(keyCodeLeft)){ input = true; }
			if(input){ buf[4]++; }else{ buf[4]=0; }
		}

		// 右 
		{
			bool input = false;
			if(Input.GetAxisRaw(axisHorizontal)>0){ input = true; }
		    if(Input.GetKey(keyCodeRight)){ input = true; }
			if(input){ buf[5]++; }else{ buf[5]=0; }
		}

		// A
		{
			bool input = false;
			if(Input.GetButton(buttonA)){ input = true; }
		    if(Input.GetKey(keyCodeA)){ input = true; }
			if(input){ buf[6]++; }else{ buf[6]=0; }
		}

		// B
		{
			bool input = false;
			if(Input.GetButton(buttonB)){ input = true; }
		    if(Input.GetKey(keyCodeB)){ input = true; }
			if(input){ buf[7]++; }else{ buf[7]=0; }
		}	

		// X
		{
			bool input = false;
			if(Input.GetButton(buttonX)){ input = true; }
		    if(Input.GetKey(keyCodeX)){ input = true; }
			if(input){ buf[8]++; }else{ buf[8]=0; }
		}

		// Y
		{
			bool input = false;
			if(Input.GetButton(buttonY)){ input = true; }
		    if(Input.GetKey(keyCodeY)){ input = true; }
			if(input){ buf[9]++; }else{ buf[9]=0; }
		}

		// L
		{
			bool input = false;
			if(Input.GetButton(buttonL)){ input = true; }
		    if(Input.GetKey(keyCodeL)){ input = true; }
			if(input){ buf[10]++; }else{ buf[10]=0; }
		}

		// R
		{
			bool input = false;
			if(Input.GetButton(buttonR)){ input = true; }
		    if(Input.GetKey(keyCodeR)){ input = true; }
			if(input){ buf[11]++; }else{ buf[11]=0; }
		}

		// LZ
		{
			bool input = false;
			//if(Input.GetButton(buttonLZ)){ input = true; }
		    if(Input.GetKey(keyCodeLZ)){ input = true; }
			if(input){ buf[12]++; }else{ buf[12]=0; }
		}

		// RZ
		{
			bool input = false;
			//if(Input.GetButton(buttonRZ)){ input = true; }
			if(Input.GetKey(keyCodeRZ)){ input = true; }
			if(input){ buf[13]++; }else{ buf[13]=0; }
		}
		
		// LS
		{
			bool input = false;
			//if(Input.GetButton(buttonLS)){ input = true; }
			if(Input.GetKey(keyCodeLS)){ input = true; }
			if(input){ buf[14]++; }else{ buf[14]=0; }
		}

		// RS
		{
			bool input = false;
			//if(Input.GetButton(buttonRS)){ input = true; }
			if(Input.GetKey(keyCodeRS)){ input = true; }
			if(input){ buf[15]++; }else{ buf[15]=0; }
		}

		// Plus
		{
			bool input = false;
			if(Input.GetButton(buttonPlus)){ input = true; }
		    if(Input.GetKey(keyCodePlus)){ input = true; }
			if(input){ buf[16]++; }else{ buf[16]=0; }
		}

		// Minus
		{
			bool input = false;
			if(Input.GetButton(buttonMinus)){ input = true; }
		    if(Input.GetKey(keyCodeMinus)){ input = true; }
			if(input){ buf[17]++; }else{ buf[17]=0; }
		}
	}

	//--------------------------------------------------------------------------------
	// 入力バッファのクリア  
	//--------------------------------------------------------------------------------
	public static void ClearInputBufffer()
	{
		for(int i=0; i<instance.Length; i++)
		{
			if(instance[i] == null){ continue; }

			for(int j=0; j<instance[i].buf.Length; j++)
			{
				instance[i].buf[j] = 0;
			}
		}
	}

#if UNITY_EDITOR
	//--------------------------------------------------------------------------------
	// DEBUG 
	//--------------------------------------------------------------------------------
	[SerializeField]bool isShowBuffer = false;
	private void OnGUI()
	{
		if(isShowBuffer)
		{
			GUILayout.Button(Input.GetAxisRaw(axisHorizontal).ToString());
			GUILayout.Button(Input.GetAxisRaw(axisVertical).ToString());

			for(int i=0; i<buf.Length; i++)
			{
	
				GUILayout.Button(buf[i].ToString());
			}
		}
	}
#endif
}
